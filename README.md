1. Install the dependencies:

   ```bash
   npm install
   ```

   or

   ```bash
   yarn install
   ```

2. Start the development server:

   ```bash
   npm run dev
   ```

   or

   ```bash
   yarn dev