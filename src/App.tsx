import {Route, Routes, useLocation} from "react-router-dom";
//importing react slick slider
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {animateScroll} from "react-scroll";

import NavBar from "./components/organs/NavBar"
import Home from "./components/pages/Home";
import {useEffect} from "react";
import Footer from "./components/organs/Footer";
import TermsofCondition from "./components/pages/TermsofCondition";
import PrivacyPolicy from "./components/pages/PrivacyPolicy";

function App() {
  const directory = useLocation();
  useEffect(() => {
    animateScroll.scrollToTop({
      duration: 0,
    });
  }, [directory.pathname]);

  return (
    <div className="w-full bg-white text-gray-950 font-poppins">
      <NavBar />
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="TermsofService" element={<TermsofCondition />} />
          <Route path="PrivacyPolicy" element={<PrivacyPolicy />} />
      </Routes>
      <Footer />
    </div>
  )
}

export default App
