export const NavLinks = [
    {
        name: "Locations",
        url: "/"
    },
    {
        name: "Services",
        url: "/"
    },

];

export const NavButtons = [
    {
        name: "Sign up",
        url: "/"
    },
    {
        name: "Login",
        url: "/"
    }

];


export const HeroTexts = {
    firstText: "BEST DESITNATIONS AROUND THE WORLD",
    secondText: "Travel, enjoy and live a new and full life.",
    thirdText: "Built Wicket longer admire do barton vantity itself do in it. Preferred to sportsmen it engrossed listening. Park gate sell they west hard for the.",
    firstButton: "Find out more",
    secondButton: 'Play Demo'
}


export const ServiceTexts = {
    firstText: "CATEGORY",
    secondText: "We Offer Best Services",
    cards: [
        {
            firstText: "Quality Professionals",
            secondText: "We've curated a community of experienced and reliable professionals who are passionate about delivering high-quality services for your home."
        },
        {
            firstText: "Ease of Use",
            secondText: "Our platform is designed to be user-friendly, ensuring that you can easily find the right service providers for your unique requirements."
        },
        {
            firstText: "Safe and Secure",
            secondText: "We prioritize your safety and security. All professionals are vetted and have a proven track record, giving you peace of mind."
        }
    ]
}

export const TopDestinationTexts = {
    firstText: "Top selling",
    secondText: "Top Destinations",
    cards: [
        {
            country: "Home Cleaning",

        },
        {
            country: "Gardner",

        },
        {
            country: "Plumber",

        },

    ]
}

export const BookingStepsTexts = {
    firstText: "Easy and Fast",
    secondText: "Book your next trip in 3 easy steps",
    listOfSteps: [
        {
            text: "Choose Destination lorem ipsum dolor sit amet, consectetur adipiscing elit.Urna, tortor tempus."
        },
        {
            text: "Make Payment lorem ipsum dolor sit amet, consectetur adipiscing elit.Uma, totor tempus."
        },
        {
            text: "Reach Airport on Selected Date lorem ipsum dolor sit amet.consectetur adipiscing elit.Uma totor tempus."
        }
    ],
    cardOne:
    {
        name: "Trip To Greece",
        date: "14-29 June  |  by Robbin Jobs",
        people: "24 people going"
    },
    cardTwo: {
        status: "Ongoing",
        destination: "Trip to Rome",
        completion: "40% Completed"
    }

}

export const TestimonialTexts = {
    firstText: "TESTIMONIALS",
    secondText: "What people say about Us.",
    feedBacks: [
        {
            text: "You did a fabulous job. There were no hitches. Thank you again for all your work planning this trip.",
            person: "Mike taylor",
            location: "Lahore, Pakistan"
        },
        {
            text: " was great with the entire process from planning to updates during the trip. We had 11 people and everything was perfectly executed. We appreciate all of her hard work. It was truly the trip of a lifetime. Thank you!",
            person: "Gina",
            location: "Owerri, Nigeria"
        },
        {
            text: "Booking through you was very easy and made our lives so much easier. I have nothing bad to say! Thank you for giving us tips and guidance before we left on what to bring and such, that was very helpful!",
            person: "Emma",
            location: "Vancouver, Canada"
        },
        {
            text: "Thank you for your recommendation and putting the trip together. The resort was absolutely beautiful. The infinity pools, the palm trees in the main pool, the infinity pool in out preferred area overlooking the gulf and the golf course were exceptional....",
            person: "Loveth",
            location: "Lagos, Nigeria"
        },
    ],
}

export const NewsletterTexts = {
    firstText: "Subscribe to get information, latest news and other interesting offers about .",
    placeholderText: "Your email",
    buttonText: "Subscribe"
}

export const FooterTexts = {
    underLogoText: "Book your trip in minutes, get full control for much longer.",
    quickLinks: {
        caption: "Quick Links",
        links: [
            {
                name: "Destination",
                url: "/"
            },
            {
                name: "Hotels",
                url: "/"
            },
            {
                name: "Flights",
                url: "/"
            },
            {
                name: "Bookings",
                url: "/"
            },
            {
                name: "Login",
                url: "/"
            },
            {
                name: "Signup",
                url: "/"
            }
        ]
    },
    contacts: {
        caption: "Contacts",
        links: [
            {
                name: "FAQs",
                url: "/"
            },
            {
                name: "Help",
                url: "/"
            },
            {
                name: "Policies",
                url: "/"
            },
            {
                name: "Terms & Conditions",
                url: "/"
            }
        ]
    },
    more: {
        caption: "More",
        links: [
            {
                name: "Career",
                url: "/"
            },
            {
                name: "Airlines",
                url: "/"
            },
            {
                name: "Airline Fees",
                url: "/"
            },
            {
                name: "Low Fare Tips",
                url: "/"
            }
        ]
    }
}