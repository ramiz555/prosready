import imgSearch from "../../assets/akar-icons_search.png";
import arrowleft from "../../assets/arrowleft.png";
import imgHand from "../../assets/iconoir_open-select-hand-gesture.png";
import imgHappy from "../../assets/ion_happy-outline.png";
import imgSymbols from "../../assets/material-symbols_book-outline.png";
import { Text } from "../atoms/Text";
// import appstore from '../../assets/appstore.png';

const Timeline = () => {
    return (

        <section className="section-timeline">
            <Text as="h1" className="lg:text-5xl md:text-3xl text-4xl text-color3 font-medium howitworkTxt">
                How It Works
            </Text>
            <div className="container">
                <img src={arrowleft} className={"arrowicon"}/>
                <ul className="timeline">

                    <li className="">
                        <div className="img-content-right" >
                            <span className="img-span">
                            <img src={imgSearch}/>
                            </span>
                        </div>
                        <div className="content">
                            <h3>Search</h3>
                            <p>
    Begin your journey by searching for the services you need. Our platform allows you to browse through a diverse range of home service providers.
</p>

                        </div>
                    </li>
                    <li className="">
                        <div className="img-content-left">
                            <span className="img-span">
                            <img src={imgHand}/>
                            </span>
                        </div>
                        <div className="content">
                            <h3>Select</h3>
                            <p>Choose the professional who best matches your requirements. Read reviews and ratings to make an informed decision.</p>
                        </div>
                    </li>
                    <li className="">
                        <div className="img-content-right">
                            <span className="img-span">
                            <img src={imgHappy}/>
                            </span>
                        </div>
                        <div className="content">
                            <h3>Book</h3>
                            <p>It's as simple as selecting a date and time that suits you. Our booking process is straightforward and efficient.</p>
                        </div>
                    </li>
                    <li className="">
                        <div className="img-content-left" >
                            <span className="img-span" >
                            <img src={imgSymbols}/>
                            </span>
                        </div>
                        <div className="content">
                            <h3>Enjoy</h3>
                            <p>Sit back and relax while the professionals take care of your home. Trust that they'll do the job to your satisfaction.</p>
                        </div>
                    </li>

                </ul>
            </div>
        </section>
    )
}

export default Timeline