import { Fade } from 'react-awesome-reveal';
import bgImage from '../../assets/HeroVector.png';
import applelg from '../../assets/apple-iphone-15-pro-lg.png';
import applesm from '../../assets/apple-iphone-15-pro.png';
import appstore from '../../assets/appstore.png';
import google from '../../assets/google.png';
import profile from '../../assets/profile.png';
import { Button } from '../atoms/Button';
import { Image } from '../atoms/Image';
import { Text } from '../atoms/Text';

const HeroSection = () => {
    return (
        <section className="w-full custom-height-lg custom-height-md custom-height relative flex justify-end sectionfirst">
            <Image className="h-[60%] w-[80%] lg:h-[90vh] md:h-[50vh] lg:w-1/2 md:w-[55%] bgImg" image={bgImage} alt="Hero Background Vector" />
            <main className="w-full lg:h-full h-auto grid md:grid-cols-1 lg:grid-cols-2 absolute top-0 left-0 lg:px-24 md:px-8 px-5 pt-24 md:pt-32 lg:pt-0">
                <div className="flex flex-col justify-center md:order-1 order-2">
                    <Text as="h1" className="lg:text-5xl md:text-5xl text-3xl font-medium headingSection1">
                        <Fade>Welcome to ProsReady - Your Ultimate Home Service Connection</Fade>
                    </Text>
                    <Text as="p" style={{
                        fontFamily:"Inter"
                    }} className="md:text-base text-sm text-justify font-light ptag text-white">
                        <Fade>At ProsReady, we're here to simplify your life and take the hassle out of finding the right home services for your needs. Our platform connects you with a network of trusted professionals, making it easier than ever to maintain and enhance your home. Whether it's a quick fix, a major renovation, or simply maintaining your space, we've got you covered.</Fade>
                    </Text>
                    <div className="w-full flex md:justify-start justify-center items-center lg:gap-12 md:gap-6 gap-0">
                        <Button type="button" className="lg:px-7 googleLogin">
                            <Image className="" image={google} alt="Hero Background Vector" />
                        </Button>
                    </div>
                    <div className="w-full flex md:justify-start justify-center items-center lg:gap-12 md:gap-6 gap-0">
                        <Button type="button" className="lg:px-7 px-5 py-3">
                            <Image className="" image={appstore} alt="Hero Background Vector" />
                        </Button>
                    </div>
                </div>
                <div className="w-full flex md:justify-start justify-center items-center lg:gap-4">
                    <img className="applelg" src={applelg} alt="Hero Background Vector"/>

                    <div>
                    <img style={{
                        position:"absolute"
                    }} className="img-1" src={applesm}  alt="Hero Background Vector"/>
                        <img className="img-2" src={profile} alt="Hero Background Vector"/>
                    </div>
                    {/*<Image className=" profile" image={profile} alt="Hero Background Vector" />*/}

                </div>
            </main>
        </section>
    );
};

export default HeroSection;
