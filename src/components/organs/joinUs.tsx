import { useState } from 'react';
import { Text } from '../atoms/Text';

const JoinUs = () => {

    const [formData, setFormData] = useState({ email: '', number: '' });

    const handleInputChange = (e:any) => {
        const { name, value } = e.target;
        setFormData({ ...formData, [name]: value });
    };

    const handleSubmit = (e:any) => {
        e.preventDefault();
        // Handle form submission, you can do something with formData here
    };
    return (
        <section className="w-full h-auto flex flex-col items-start justify-center relative lg:px-24 md:px-10 px-6 my-28 gap-5">

            <main className='w-full grid md:grid-cols-2 lg:gap-6 gap-12 md:gap-5'>
                {/* Text and Steps Container  */}
                <div className="cardSeperate">
                <div className='w-full flex flex-col gap-6 order-2 md:order-1'>
                    <Text as="h1" className="lg:text-5xl md:text-3xl text-4xl text-color3 font-medium joinusTxt">
                        Join Our Community
                    </Text>
                    <p>Join the countless homeowners who have made their lives easier with [Your Platform Name]. Whether you're a homeowner seeking services or a skilled professional looking to join our network, we're here to connect you.</p>

                </div>
                </div>

                {/* Card Container  */}
                <div className='w-full flex flex-col gap-6 order-1 md:order-2'>
                    <Text as="h1" className="lg:text-5xl md:text-3xl text-4xl text-color3 font-medium joinusTxt">
                        Contact Us
                    </Text>
                    <p>For any inquiries or assistance, our dedicated team is here to help. Reach out to us today:</p>


                    <form onSubmit={handleSubmit}>
                            <input
                                type="email"
                                name="email"
                                className={"rectangleEmail"}
                                placeholder="Email"
                                value={formData.email}
                                onChange={handleInputChange}
                            />
                        <br />
                            <input
                                type="number"
                                name="number"
                                className={"rectanglenumber"}
                                placeholder="Contact Number"
                                value={formData.number}
                                onChange={handleInputChange}
                            />
                        <br />
                        <textarea
                            className={"rectangletextarea"}
                            placeholder="Type here"
                            rows={5} // Set the number of rows
                            cols={10} // Set the number of columns
                        ></textarea>
                        <div className="container">
                            <button type="submit" className="contactbtn">Submit</button>
                        </div>
                    </form>
                </div>

            </main>
        </section>
    )
}

export default JoinUs