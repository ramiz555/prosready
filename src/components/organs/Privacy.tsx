import { Text } from '../atoms/Text';

const Privacy = () => {


    return (
        <section className="w-full h-auto flex flex-col items-start justify-center relative lg:px-24 md:px-10 px-6 my-28 gap-5">

            <main className='w-full grid  lg:gap-6 gap-12 md:gap-5'>
                {/* Text and Steps Container  */}
                <div className="cardSeperate">
                <div className='w-full flex flex-col gap-6 order-2 md:order-1'>
                    <Text as="h1" className="lg:text-5xl md:text-3xl text-4xl text-color3 font-medium termscondition">
                        Privacy Policy
                    </Text>
                    <p>This Privacy Policy (the "Policy") is designed to help you understand how ProsReady, a [legal entity type, e.g., corporation/LLC], collects, uses, and safeguards your personal information when you use our platform and services. By accessing or using ProsReady's services, you consent to the practices outlined in this Policy.

                        1. Information We Collect

                        a. User Information: When you use our platform, we may collect information such as your name, email address, phone number, and other information necessary for registration and communication.

                        b. Service Information: Information related to services you request or provide through our platform, such as service descriptions, transaction details, and service ratings.

                        c. Device and Usage Information: We may collect information about the devices you use to access our platform, including your IP address, browser type, operating system, and usage data.

                        2. How We Use Your Information

                        a. Service Provision: We use your information to facilitate the booking and provision of services between Pros and Users. This includes matching Users with Pros, processing payments, and enabling communication.

                        b. Communication: We may use your contact information to send you transaction confirmations, support communications, service updates, and promotional materials, where allowed by law.

                        c. Improvement: We use data analytics and feedback to improve our platform, services, and user experience.

                        3. Sharing Your Information

                        a. With Pros: We share your information with Pros to facilitate service bookings and communication. Pros may receive your name, contact information, and service details.

                        b. With Third Parties: We may share your information with third-party service providers who help us with payment processing, analytics, and marketing. These third parties are contractually obligated to protect your information.

                        c. Legal and Safety: We may disclose your information to comply with legal obligations, protect our rights, safety, or the rights and safety of others.

                        4. Your Choices

                        a. Access and Update: You can access and update your personal information through your account settings.

                        b. Marketing Communications: You can opt out of receiving promotional emails from us at any time by following the instructions provided in those emails.

                        5. Security

                        We take reasonable measures to protect your information from unauthorized access and maintain the security of your data. However, no method of transmission or storage is completely secure, and we cannot guarantee absolute security.

                        6. Data Retention

                        We retain your information for as long as necessary to provide our services and as required by applicable laws. If you want your information to be deleted, please contact us.

                        7. Children's Privacy

                        Our platform is not intended for use by individuals under the age of 18. We do not knowingly collect personal information from minors.

                        8. Changes to this Policy

                        We may update this Privacy Policy from time to time. Any changes will be posted on our platform, and the date of the latest revision will be noted. Your continued use of our services after such changes constitutes your acceptance of the updated Policy.

                        9. Contact Us

                        For questions or concerns related to this Privacy Policy or our data practices, please contact us at:</p>

                </div>
                </div>

            </main>
        </section>
    )
}

export default Privacy