import {useCallback} from "react";
import {Text} from "../atoms/Text"
import {TopDestinationTexts} from "../particles/DataLists"
import {Card} from "../molecules/Card";
import City1 from "../../assets/gallery1.png"
import City2 from "../../assets/gallery2.png"
import City3 from "../../assets/gallery3.png"


const Services = () => {


    // Function for next button
    

    const renderCities = useCallback((element: number) => {
        switch (element) {
            case 0:
                return City1;
            case 1:
                return City2;
            case 2:
                return City3;
            default:
                return "";
        }
    }, [])

    return (
        <section className="w-full h-auto flex flex-col items-center justify-center relative lg:px-24 md:px-20 px-6 my-20">
            <Text as="h1" className="md:text-4xl text-2xl font-medium capitalize text-color3 section2Heading">
                Services
            </Text>

            {/* Controllers  */}
            {/*<div className="mt-12 w-full flex justify-end gap-5 items-center md:px-6 px-3">*/}
                {/*<Button onClick={previous} className="cursor-pointer outline-none border-none bg-color2/30 text-color3 hover:bg-color2 p-2 rounded-full" type="button">*/}
                    {/*<CaretLeft size={18} color="currentColor" weight="fill" />*/}
                {/*</Button>*/}
                {/*<Button onClick={next} className="cursor-pointer outline-none border-none bg-color2/30 text-color3 hover:bg-color2 p-2 rounded-full" type="button">*/}
                    {/*<CaretRight size={18} color="currentColor" weight="fill" />*/}
                {/*</Button>*/}
            {/*</div>*/}

            {/* Slides  */}
            <div className="w-full h-auto grid lg:grid-cols-3 md:grid-cols-3 lg:gap-7 md:gap-10 gap-7 my-12 z-20 px-8 md:px-0">
                    {
                        TopDestinationTexts.cards.map((card, index) => (
                            <div key={index} className="md:px-6 px-3">
                                <Card cardClass="overflow-hidden shadow-md rounded-lg cursor-pointer group" imageAlt={card.country} imageSrc={renderCities(index)} imageWrapperClass="w-full h-[250px] overflow-hidden" cover="group-hover:scale-125 transition duration-500 ease" textWrapperClass="flex flex-col gap-4 w-full px-5 py-5">

                                        <Text as="h4" className="text-base text-center font-medium text-color3">
                                            {card.country}
                                        </Text>

                                </Card>
                            </div>
                        ))
                    }

            </div>
            <button className={"moreServices"}>View more services</button>
        </section>
    )
}

export default Services