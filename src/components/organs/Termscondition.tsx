import { Text } from '../atoms/Text';

const Termscondition = () => {


    return (
        <section className="w-full h-auto flex flex-col items-start justify-center relative lg:px-24 md:px-10 px-6 my-28 gap-5">

            <main className='w-full grid lg:gap-6 gap-12 md:gap-5'>
                {/* Text and Steps Container  */}
                <div className="cardSeperate">
                <div className='w-full flex flex-col gap-6 order-2 md:order-1'>
                    <Text as="h1" className="lg:text-5xl md:text-3xl text-4xl text-color3 font-medium termscondition">
                        Terms of Service Agreement
                    </Text>
                    <p>This Terms of Service Agreement (the "Agreement") is entered into by and between ProsReady, a [legal entity type, e.g., corporation/LLC], and any individuals or entities who use the ProsReady platform (collectively referred to as "Users"). This Agreement sets forth the terms and conditions under which Users may access and use ProsReady's services.

                        1. Acceptance of Terms

                        By accessing or using ProsReady's platform and services, you acknowledge and agree to abide by the terms and conditions outlined in this Agreement. If you do not agree with these terms, you must not use our services.

                        2. The ProsReady Platform

                        a. Service Providers (Pros): ProsReady allows individuals or entities to offer services as independent contractors ("Pros") to Users seeking such services. Pros are not employees of ProsReady.

                        b. User Responsibilities: Users agree to provide accurate information and comply with all applicable laws and regulations while using the platform. Users must not engage in any illegal, harmful, or fraudulent activities.

                        3. Booking Services

                        a. Service Requests: Users can request services from Pros through the ProsReady platform. The Pros may accept or decline such requests.

                        b. Service Fee: Users agree to pay the fees associated with the services they book, as outlined on the platform. ProsReady may charge a service fee for connecting Users with Pros.

                        4. Pros' Services

                        a. Pros' Obligations: Pros agree to perform the requested services professionally, safely, and in compliance with all relevant laws and regulations. Pros are responsible for their own tools, equipment, and transportation.

                        b. Payment: Pros agree to accept payment through ProsReady's platform for the services they provide. Payment will be processed as specified on the platform.

                        5. Cancellations and Refunds

                        a. Cancellation Policy: Pros and Users must adhere to ProsReady's cancellation policy, which is outlined on the platform.

                        b. Refunds: Refunds, if applicable, will be processed in accordance with ProsReady's refund policy, also outlined on the platform.

                        6. User and Pro Ratings

                        a. Rating System: Pros and Users are encouraged to provide honest and constructive feedback through the platform's rating system. ProsReady may use these ratings to improve the platform and may suspend or terminate the accounts of Users or Pros with consistently low ratings.

                        7. Privacy and Data

                        a. Privacy: ProsReady collects and uses personal information in accordance with its Privacy Policy, available on the platform.

                        8. Termination and Suspension

                        a. Termination: ProsReady may, at its discretion, terminate or suspend Users' or Pros' access to the platform for violations of this Agreement or for any other reason.

                        9. Disclaimers and Limitations of Liability

                        a. Disclaimer: ProsReady makes no warranties or representations regarding the quality, safety, or legality of services provided by Pros. ProsReady is not responsible for any actions or omissions of Users or Pros.

                        b. Limitation of Liability: To the maximum extent permitted by law, ProsReady is not liable for any direct, indirect, incidental, special, or consequential damages.

                        10. Amendments

                        a. Changes to Agreement: ProsReady reserves the right to amend this Agreement at any time. Users will be notified of such changes, and continued use of the platform constitutes acceptance of the modified Agreement.

                        11. Governing Law and Jurisdiction

                        a. Governing Law: This Agreement is governed by and construed in accordance with the laws of [Your Jurisdiction].

                        12. Contact Information

                        a. Contact: For any questions or concerns regarding this Agreement, please contact ProsReady at [Contact Information].

                        By using the ProsReady platform, Users and Pros acknowledge and agree to be bound by the terms and conditions outlined in this Agreement.</p>

                </div>
                </div>

            </main>
        </section>
    )
}

export default Termscondition