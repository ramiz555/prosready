import {Image} from "../atoms/Image"
import instagram from "../../assets/instagram.png"
import youtube from "../../assets/youtube.png"
import game from "../../assets/game.png"
import lin from "../../assets/lin.png"
import logo from "../../assets/logo2.png"

import {NavLink} from "react-router-dom";


const Footer = () => {
    return (
        <footer className="w-full flex flex-col bg-white">
            <section className="w-full h-auto  lg:px-20 md:px-12 px-6 py-16 gap-7 md:gap-4 lg:gap-0 footercolor">
                <div className=" w-full h-auto grid lg:grid-cols-1 lg:gap-12 md:gap-30 gap-12 footerimgmain">
                    <div className="lg:flex lg:items-center lg:justify-center">
                        <Image className="w-28 " image={logo} alt="Logo" as="a" href="/" />
                    </div>
                    {/* Add more grid items for the second column as needed */}
                </div>
                <div className={"footerSeperate"}> </div>
                <div className="footerend w-full h-auto grid lg:grid-cols-2 md:grid-cols-5 lg:gap-1 md:gap-10 gap-3">

                <div className={"terms"}>
                    <NavLink   style={{ color: 'white' }} to={'/TermsofService'} className="btncs footertermsSeperate">Terms of use</NavLink>
                        &nbsp;&nbsp;
                    <NavLink   style={{ color: 'white' }} to={'/PrivacyPolicy'} className="btncs">Privacy Policy</NavLink>
                    </div>
                    <div className="w-full h-auto flex items-center justify-center social ">
                        <Image className="w-28 " image={instagram} alt="Instagram" as="a" href="/" />
                        <Image className="w-28 " image={youtube} alt="YouTube" as="a" href="/" />
                        <Image className="w-28 " image={game} alt="Game" as="a" href="/" />
                        <Image className="w-28 " image={lin} alt="LinkedIn" as="a" href="/" />
                    </div>
                </div>


            </section>

        </footer>
    )
}

export default Footer