import Termscondition from "../organs/Termscondition"
import HeroSection from "../organs/HeroSection";


const TermsofCondition = () => {
    return (
        <>
            <HeroSection />
            <Termscondition />

        </>
    )
}

export default TermsofCondition