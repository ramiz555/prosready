import Privacy from "../organs/Privacy"
import HeroSection from "../organs/HeroSection";


const PrivacyPolicy = () => {
    return (
        <>
            <HeroSection />
            <Privacy />

        </>
    )
}

export default PrivacyPolicy