import HeroSection from "../organs/HeroSection";
import ProsReady from "../organs/ProsReady";
import Services from "../organs/Services";
import Timeline from "../organs/Timeline";
import JoinUs from "../organs/JoinUs";


const Home = () => {
    return (
        <>
            <HeroSection />
            <ProsReady />
            <Services />
            <Timeline/>
            <JoinUs />

        </>
    )
}

export default Home